import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material';
import { NavbarComponent } from './navbar/navbar.component';
import { CarouselComponent } from './carousel/carousel.component';

@NgModule({
  declarations: [NavbarComponent, CarouselComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatSidenavModule
  ],
  exports: [
    NavbarComponent,
    CarouselComponent,
  ]
})
export class LayoutModule { }

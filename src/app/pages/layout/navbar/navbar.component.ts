import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Output() acaoAbrirSideNav = new EventEmitter();

  constructor() { }

  ngOnInit() {  }
  open() {
    this.acaoAbrirSideNav.emit();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InicioComponent} from './pages/inicio/inicio.component';
import {GaleriaComponent} from './pages/galeria/galeria.component';
import {NossaHistoriaComponent} from './pages/nossa-historia/nossa-historia.component';
import {ConfirmarPresencaComponent} from './pages/confirmar-presenca/confirmar-presenca.component';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'galeria', component: GaleriaComponent },
  { path: 'nossa-historia', component: NossaHistoriaComponent },
  { path: 'confirmar-presenca', component: ConfirmarPresencaComponent },
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
